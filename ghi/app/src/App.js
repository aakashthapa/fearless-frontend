import AttendeesList from './AttendeesList';
import Nav from './Nav'; // Import the Nav component
import LocationForm from './LocationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav /> 
      <div className="container">
        <LocationForm/>
        <AttendeesList  attendees={props.attendees} />{/* Pass attendees as a prop */}
      </div>
    </>
  );
}

export default App;
